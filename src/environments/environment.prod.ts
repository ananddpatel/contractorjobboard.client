export const environment = {
  production: true,
  apiBase: 'https://cjb-api.herokuapp.com',
  registerResource: '/user/register',
  loginResource: '/user/authenticate',
  profileResource: '/user/profile',
  postJobResource: '/job'
};
