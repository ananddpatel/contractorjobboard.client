import { Injectable } from '@angular/core';

@Injectable({providedIn: 'root'})
export class U {
  static toArray(obj: Object): any[] {
    const _return = [];
    Object.keys(obj).forEach(k => {
      return _return.push(obj[k]);
    });
    return _return;
  }

  static objFilter(obj: Object, fx: Function): Object {
    const _return = {};
    Object.keys(obj).forEach(k => {
      if (fx(obj[k])) {
        _return[k] = (obj[k]);
      }
    });
    return _return;
  }

  /**
   * give date in YYYY-MM-DD
   * @param date in ISO Format
   * @return date in YYYY-MM-DD format
   */
  static isoDateInputFormat(date: string): string {
    if (isNaN(Date.parse(date))) {
      return '';
    }
    return date.split('T')[0];
    // const d = new Date(date);
    // return `${d.getFullYear()}-${d.getMonth() + 1}-${d.getDate()}`;
  }
}
