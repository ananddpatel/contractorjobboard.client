export enum SeekerAvailablity {
  NOT_AVAILABLE = 'notAvailable',
  IMMEDIATELY = 'immediately',
  IN_2_WEEKS = 'in2Weeks',
  IN_1_MONTH = 'in1Month'
}
