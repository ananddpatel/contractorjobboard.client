export enum UserRoles {
  SEEKER = 'seeker',
  AGENCY = 'agency',
  RECRUITER = 'recruiter',
}
