export enum Messages {
  POST_JOB_SUCCESS = 'Job submitted.',
  POST_JOB_FAIL_EXISTS = 'You\'ve already submitted a job with this Job Id.',
  POST_JOB_FAIL_DEFAULT = 'An error occured while submitting this job. Please try again later.'
}
