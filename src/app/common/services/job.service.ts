import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'environments/environment';
import { Observable, throwError } from 'rxjs';
import { JobPostRequest } from '../models/job-post-request.model';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class JobService {

  constructor(private http: HttpClient) {}

  postJob(job: JobPostRequest): Observable<any> {
    const url = environment.apiBase + environment.postJobResource;
    return this.http.post(url, job, {
      headers: {'Authorization': `Bearer ${localStorage.getItem('token')}`}
    }).pipe(
      catchError((err: HttpErrorResponse) => throwError(err.error))
    );
  }
}
