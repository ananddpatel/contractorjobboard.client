import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';
import { UserProfile } from '../models/user-profile.model';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(private http: HttpClient) {}

  getProfile(): Observable<any> {
    const url = environment.apiBase + environment.profileResource;
    return this.http.get(url, {
      headers: {'Authorization': `Bearer ${localStorage.getItem('token')}`}
    });
  }

  updateProfile(profileDate: UserProfile): Observable<any> {
    const url = environment.apiBase + environment.profileResource;
    return this.http.put(url, profileDate, {
      headers: {'Authorization': `Bearer ${localStorage.getItem('token')}`}
    });
  }
}
