export interface JobPostRequest {
  jobId: string;
  title: string;
  description: string;
  isConfidential: boolean;
  employer: string;
  startDate?: string;
  endDate?: string;
  minRate: number;
  maxRate: number;
  skills: string[];
  applicationCloseDate?: any;
}
