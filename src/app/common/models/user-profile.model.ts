export interface UserProfile {
  email: string;
  phone: string;
  currEmployer: string;
  currContractStart: string;
  currContractEnd: string;
  availability: string;
  linkedInProfile: string;
  dob: string;
  location: string;
  preferredPayRate: string;
  preferredDesignation: string[];
  preferredIndustry: string[];
  preferredLocation: string[];
  firstName: string;
  lastName: string;
}
