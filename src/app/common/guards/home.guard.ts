import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { isTokenExpired, role } from '@a-redux/selectors/root.selectors';
import { take, map } from 'rxjs/operators';
import { combineLatest } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HomeGuard implements CanActivate {
  constructor(private store: Store<any>, private router: Router) { }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> {
      return this.store.pipe(select(isTokenExpired), take(1), map(isExpired => {
        if (!isExpired) {
          this.router.navigate(['/search']);
          return false;
        }
        return true;
      }));
  }
}
