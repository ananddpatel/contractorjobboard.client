import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { isTokenExpired, role } from '@a-redux/selectors/root.selectors';
import { take, map } from 'rxjs/operators';
import { combineLatest } from 'rxjs';
import { UserRoles } from '../enums/user-roles.enum';

@Injectable({
  providedIn: 'root'
})
export class SeekerHomeGuard implements CanActivate {
  constructor(private store: Store<any>, private router: Router) { }

  // sends to search page if user is logged in tries to go to a seeker route if theyre not a seeker
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> {
      return this.store.pipe(select(role), take(1), map(r => {
        // console.log(role);
        if (!r) {
          this.router.navigate(['/search']);
          return false;
        }
        if (r !== UserRoles.SEEKER) {
          this.router.navigate(['/search']);
          return false;
        }
        return true;
      }));
  }
}
