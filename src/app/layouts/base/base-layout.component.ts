import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { isTokenExpired } from '@a-redux/selectors/root.selectors';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-base-layout',
  templateUrl: './base-layout.component.html',
  styleUrls: ['./base-layout.component.css']
})
export class BaseLayoutComponent implements OnInit {
  tokenExpired$: Observable<boolean>;

  constructor(private store: Store<any>) { }

  ngOnInit() {
    this.tokenExpired$ = this.store.pipe(select(isTokenExpired));
  }

}
