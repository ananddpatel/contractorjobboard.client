import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routes';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { SharedModule } from './shared-module/shared.module';
import { BaseLayoutComponent } from './layouts/base/base-layout.component';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { metaReducers, rootReducers } from '@a-redux/all-reducers-maps';
import { EffectsModule } from '@ngrx/effects';
import { NotFoundPageComponent } from './pages/not-found-page/not-found-page.component';
import { UserEffects } from '@a-redux/effects/user.effect';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ElementsModule } from './elements/elements.module';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    BaseLayoutComponent,
    NotFoundPageComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    EffectsModule.forRoot([UserEffects]),
    StoreModule.forRoot(rootReducers, { metaReducers }),
    StoreDevtoolsModule.instrument({ maxAge: 5 })
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule { }
