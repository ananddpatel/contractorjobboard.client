import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { BaseLayoutComponent } from './layouts/base/base-layout.component';
import { NotFoundPageComponent } from './pages/not-found-page/not-found-page.component';
import { RecuiterHomeGuard } from './common/guards/recruiter-home.guard';
import { HomeGuard } from './common/guards/home.guard';
import { SeekerHomeGuard } from './common/guards/seeker-home.guard';

const routes: Routes = [{
  path: '', component: BaseLayoutComponent, children: [
    { path: '', component: HomePageComponent, canActivate: [HomeGuard] },
    {
      path: '',
      loadChildren: '../app/pages/auth/auth.module#AuthModule'
    },
    {
      path: '',
      loadChildren: '../app/pages/search/search.module#SearchModule'
    },
    {
      path: '',
      loadChildren: '../app/pages/seeker/seeker.module#SeekerModule',
      canActivate: [SeekerHomeGuard]
    },
    {
      path: '',
      loadChildren: '../app/pages/recruiter/recruiter.module#RecruiterModule',
      canActivate: [RecuiterHomeGuard]
    },
    { path: '**', component: NotFoundPageComponent },
  ]
}];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      useHash: true
    })
  ],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule { }

export const routedComponents = [AppComponent, HomePageComponent];
