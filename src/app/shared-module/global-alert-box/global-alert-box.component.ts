import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { getGlobalAlertMessage, getGlobalAlertStatus } from '@a-redux/selectors/alert.selectors';
import { Observable } from 'rxjs';
import { ResetAlert } from '@a-redux/actions/alert.actions';
import { Status } from '../../common/enums/status.enum';

@Component({
  selector: 'app-global-alert-box',
  templateUrl: './global-alert-box.component.html',
  styleUrls: ['./global-alert-box.component.css']
})
export class GlobalAlertBoxComponent implements OnInit {
  status = Status;
  message$: Observable<string>;
  status$: Observable<Status>;

  currentTimeout: number;

  constructor(private store: Store<any>) {
    this.message$ = this.store.pipe(select(getGlobalAlertMessage));
    this.status$ = this.store.pipe(select(getGlobalAlertStatus));
  }

  ngOnInit() {
    this.message$.subscribe(d => {
      if (d) {
        this.currentTimeout = window.setTimeout(() => {
          this.reset();
          window.clearTimeout(this.currentTimeout);
        }, 4000);
      }
    });
  }

  reset() {
    this.store.dispatch(new ResetAlert());
  }

}
