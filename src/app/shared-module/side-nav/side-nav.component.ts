import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Logout } from '@a-redux/actions/login.actions';
import { Observable } from 'rxjs';
import { UserProfile } from '../../common/models/user-profile.model';
import { rootPropertySelector, userProfile, role } from '@a-redux/selectors/root.selectors';
import { UserRoles } from '../../common/enums/user-roles.enum';

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.css']
})
export class SideNavComponent implements OnInit {
  userRoles = UserRoles;
  profile$: Observable<UserProfile>;
  role$: Observable<UserRoles>;

  constructor(private store: Store<any>) {
    this.profile$ = this.store.pipe(select(userProfile));
    this.role$ = this.store.pipe(select(role));
  }

  ngOnInit() {
  }

  logout() {
    this.store.dispatch(new Logout());
  }
}
