import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { RouterModule } from '@angular/router';
import { SideNavComponent } from './side-nav/side-nav.component';
import { GlobalAlertBoxComponent } from './global-alert-box/global-alert-box.component';
import { StoreModule } from '@ngrx/store';

@NgModule({
  imports: [
    CommonModule,
    StoreModule,
    RouterModule,
  ],
  exports: [HeaderComponent, SideNavComponent, GlobalAlertBoxComponent],
  declarations: [HeaderComponent, SideNavComponent, GlobalAlertBoxComponent]
})
export class SharedModule { }
