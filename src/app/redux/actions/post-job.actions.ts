import { Action } from '@ngrx/store';
import { JobPostRequest } from '../../common/models/job-post-request.model';

export const POST_JOB = '[Recruiter] Post Job';
export const POST_JOB_SUCCESS = '[Recruiter] Post Job Success';
export const POST_JOB_FAIL = '[Recruiter] Post Job Fail';

export class PostJob implements Action {
  readonly type = POST_JOB;
  constructor(public job: JobPostRequest) {}
}

export class PostJobSuccess implements Action {
  readonly type = POST_JOB_SUCCESS;
}
export class PostJobFail implements Action {
  readonly type = POST_JOB_FAIL;
  constructor(public error: any) {}
}

export type Actions = PostJob | PostJobSuccess | PostJobFail;
