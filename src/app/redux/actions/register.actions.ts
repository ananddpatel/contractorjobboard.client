import { Action } from '@ngrx/store';

export const REGISTER_USER = '[Register] Register User';
export const REGISTER_USER_SUCCESS = '[Register] Register User Success';
export const REGISTER_USER_FAIL = '[Register] Register User Fail';

export class RegisterUser implements Action {
  readonly type = REGISTER_USER;
  constructor(public userData: any) {}
}

export class RegisterUserSuccess implements Action {
  readonly type = REGISTER_USER_SUCCESS;
}
export class RegisterUserFail implements Action {
  readonly type = REGISTER_USER_FAIL;
}

export type Actions = RegisterUser | RegisterUserSuccess | RegisterUserFail;
