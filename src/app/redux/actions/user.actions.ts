import { Action } from '@ngrx/store';
import { UserProfile } from '../../common/models/user-profile.model';

export const GET_USER_PROFILE = '[User] Get User Profile';
export const GET_USER_PROFILE_SUCCESS = '[User] Get User Profile Success';
export const GET_USER_PROFILE_FAIL = '[User] Get User Profile Fail';

export const UPDATE_USER_PROFILE = '[User] Update User Profile';
export const UPDATE_USER_PROFILE_SUCCESS = '[User] Update User Profile Success';
export const UPDATE_USER_PROFILE_FAIL = '[User] Update User Profile Fail';

export class GetUserProfile implements Action {
  readonly type = GET_USER_PROFILE;
}
export class GetUserProfileSuccess implements Action {
  readonly type = GET_USER_PROFILE_SUCCESS;
  constructor(public profile: UserProfile) {}
}
export class GetUserProfileFail implements Action {
  readonly type = GET_USER_PROFILE_FAIL;
}

export class UpdateUserProfile implements Action {
  readonly type = UPDATE_USER_PROFILE;
  constructor(public profile: UserProfile) {}
}
export class UpdateUserProfileSuccess implements Action {
  readonly type = UPDATE_USER_PROFILE_SUCCESS;
  constructor(public profile: UserProfile) {}
}
export class UpdateUserProfileFail implements Action {
  readonly type = UPDATE_USER_PROFILE_FAIL;
}

export type Actions = GetUserProfile
  | GetUserProfileSuccess
  | GetUserProfileFail
  | UpdateUserProfile
  | UpdateUserProfileSuccess
  | UpdateUserProfileFail;
