import { Action } from '@ngrx/store';

export const ALERT_SUCCESS = '[Alert] Alert Success';
export const ALERT_FAIL = '[Alert] Alert Fail';
export const RESET_ALERT = '[Alert] Reset';

export class AlertSuccess implements Action {
  readonly type = ALERT_SUCCESS;
  constructor(public message: string) {}
}

export class AlertFail implements Action {
  readonly type = ALERT_FAIL;
  constructor(public message: string) {}
}

export class ResetAlert implements Action {
  readonly type = RESET_ALERT;
}

export type Actions = AlertSuccess | AlertFail | ResetAlert;
