import { Action } from '@ngrx/store';
import { UserRoles } from '../../common/enums/user-roles.enum';

export const SET_USER_DATA = '[Root] Set User Data';

export class SetUserData implements Action {
  readonly type = SET_USER_DATA;
  constructor(public role: UserRoles, public expiresAt: number) {}
}

export type Actions = SetUserData ;
