import { Action } from '@ngrx/store';

export const LOGIN_USER = '[Login] Login User';
export const LOGIN_USER_SUCCESS = '[Login] Login User Success';
export const LOGIN_USER_FAIL = '[Login] Login User Fail';

export const LOGOUT_USER = '[Logout] Logout User';

export class LoginUser implements Action {
  readonly type = LOGIN_USER;
  constructor(public userData: any) {}
}

export class LoginUserSuccess implements Action {
  readonly type = LOGIN_USER_SUCCESS;
}

export class LoginUserFail implements Action {
  readonly type = LOGIN_USER_FAIL;
}

export class Logout implements Action {
  readonly type = LOGOUT_USER;
}

export type Actions = LoginUser | LoginUserSuccess | LoginUserFail | Logout;
