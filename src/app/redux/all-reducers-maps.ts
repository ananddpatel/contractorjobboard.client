import { storeFreeze } from 'ngrx-store-freeze';
import { MetaReducer, ActionReducerMap } from '@ngrx/store';
import { environment } from '../../environments/environment';
import * as S from './all-states';
import { registerReducer } from '@a-redux/reducers/register.reducer';
import { loginReducer } from '@a-redux/reducers/login.reducer';
import { rootReducer } from '@a-redux/reducers/root.reducer';
import { postJobReducer } from '@a-redux/reducers/post-job.reducer';
import { alertReducer } from '@a-redux/reducers/alert.reducer';

export const metaReducers: MetaReducer<any>[] = !environment.production
  ? [storeFreeze]
  : [];

export const rootReducers: ActionReducerMap<S.RootState> = {
  root: rootReducer,
  alert: alertReducer
};

export const authReducers: ActionReducerMap<S.AuthState> = {
  login: loginReducer,
  register: registerReducer
};

export const recruiterReducers: ActionReducerMap<S.RecruiterState> = {
  postJob: postJobReducer
};

