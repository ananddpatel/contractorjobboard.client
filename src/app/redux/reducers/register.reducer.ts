import * as auth from '@a-redux/actions/register.actions';
import { Status } from '../../common/enums/status.enum';

export interface RegisterState {
  status: Status;
}

const initialState = {
  status: Status.NOT_STARTED
};

export function registerReducer(
  state: RegisterState = initialState,
  action: auth.Actions
) {
  switch (action.type) {
    case auth.REGISTER_USER:
      return { ...state, status: Status.IN_PROGRESS};

    case auth.REGISTER_USER_SUCCESS:
      return { ...state, status: Status.COMPLETED};

    case auth.REGISTER_USER_FAIL:
      return { ...state, status: Status.FAILED};

    default:
      return state;
  }
}
