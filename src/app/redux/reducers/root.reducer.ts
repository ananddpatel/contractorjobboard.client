import * as root from '@a-redux/actions/root.actions';
import * as login from '@a-redux/actions/login.actions';
import * as user from '@a-redux/actions/user.actions';
import { UserRoles } from '../../common/enums/user-roles.enum';
import { UserProfile } from '../../common/models/user-profile.model';
import { Status } from '../../common/enums/status.enum';

export interface RootState {
  role: UserRoles;
  expiresAt: number;
  profile: UserProfile;
  getProfileStatus: Status;
  updateProfileStatus: Status;
}

const initialState = {
  role: null,
  expiresAt: null,
  profile: null,
  getProfileStatus: Status.NOT_STARTED,
  updateProfileStatus: Status.NOT_STARTED
};

export function rootReducer(
  state: RootState = initialState,
  action: root.Actions | login.Actions | user.Actions
) {
  switch (action.type) {
    case root.SET_USER_DATA:
      return { ...state, role: action.role, expiresAt: action.expiresAt};

    case login.LOGOUT_USER:
      return initialState;
    case user.GET_USER_PROFILE:
      return { ...state,  getProfileStatus: Status.IN_PROGRESS};
    case user.GET_USER_PROFILE_SUCCESS:
      return { ...state,  profile: action.profile, getProfileStatus: Status.SUCCESS};
    case user.GET_USER_PROFILE_FAIL:
      return { ...state, getProfileStatus: Status.FAILED};


    case user.UPDATE_USER_PROFILE:
      return { ...state,  updateProfileStatus: Status.IN_PROGRESS};
    case user.UPDATE_USER_PROFILE_SUCCESS:
      return { ...state,  profile: action.profile, updateProfileStatus: Status.SUCCESS};
    case user.UPDATE_USER_PROFILE_FAIL:
      return { ...state, updateProfileStatus: Status.FAILED};

    default:
      return state;
  }
}
