import * as alert from '@a-redux/actions/alert.actions';
import { Status } from '../../common/enums/status.enum';

export interface AlertState {
  message: string;
  status: Status;
}

const initialState = {
  message: null,
  status: null
};

export function alertReducer(
  state: AlertState = initialState,
  action: alert.Actions
) {
  switch (action.type) {
    case alert.ALERT_SUCCESS:
      return { ...state, status: Status.SUCCESS, message: action.message};

    case alert.ALERT_FAIL:
      return { ...state, status: Status.FAILED, message: action.message};

    case alert.RESET_ALERT:
      return initialState;

    default:
      return state;
  }
}
