import * as login from '@a-redux/actions/login.actions';
import { Status } from '../../common/enums/status.enum';

export interface LoginState {
  status: Status;
}

const initialState = {
  status: Status.NOT_STARTED,
};

export function loginReducer(
  state: LoginState = initialState,
  action: login.Actions
) {
  switch (action.type) {
    case login.LOGIN_USER:
      return { ...state, status: Status.IN_PROGRESS};

    case login.LOGIN_USER_SUCCESS:
      return {
        ...state,
        status: Status.COMPLETED,
      };

    case login.LOGIN_USER_FAIL:
      return { ...state, status: Status.FAILED};

    case login.LOGOUT_USER:
      return {...state, status: Status.NOT_STARTED};

    default:
      return state;
  }
}
