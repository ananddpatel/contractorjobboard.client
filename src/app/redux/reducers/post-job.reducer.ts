import * as postJob from '@a-redux/actions/post-job.actions';
import { Status } from '../../common/enums/status.enum';

export interface PostJobState {
  status: Status;
}

const initialState = {
  status: Status.NOT_STARTED,
};

export function postJobReducer(
  state: PostJobState = initialState,
  action: postJob.Actions
) {
  switch (action.type) {

    case postJob.POST_JOB:
      return { ...state, status: Status.IN_PROGRESS};
    case postJob.POST_JOB_SUCCESS:
      return { ...state, status: Status.NOT_STARTED};
    case postJob.POST_JOB_FAIL:
      return { ...state, status: Status.FAILED};

    default:
      return state;
  }
}
