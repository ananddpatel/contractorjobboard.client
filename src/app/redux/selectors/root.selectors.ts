import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as root from '@a-redux/reducers/root.reducer';
import { RootState } from '@a-redux/all-states';

export const getRootState = createSelector(
  (state: RootState) => state.root
);

export const isTokenExpired = createSelector(
  getRootState,
  (state: root.RootState) => {
    const now = new Date().getTime();
    if (state.expiresAt && now < state.expiresAt) {
      return false;
    }
    return true;
  }
);
export const role = createSelector(
  getRootState,
  (state: root.RootState) => state.role
);

export const userProfile = createSelector(
  getRootState,
  (state: root.RootState) => state.profile
);

export const profileGetStatus = createSelector(
  getRootState,
  (state: root.RootState) => state.getProfileStatus
);

export const rootPropertySelector: any = property => {
  return createSelector(getRootState, (state: any) => state[property]);
};
