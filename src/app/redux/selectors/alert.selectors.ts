import { createSelector } from '@ngrx/store';
import { RootState } from '@a-redux/all-states';
import { AlertState } from '@a-redux/reducers/alert.reducer';


export const getGlobalAlertState = createSelector(
  (state: RootState) => state.alert
);

export const getGlobalAlertMessage = createSelector(
  getGlobalAlertState,
  (state: AlertState) => state.message
);
export const getGlobalAlertStatus = createSelector(
  getGlobalAlertState,
  (state: AlertState) => state.status
);
