import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as register from '@a-redux/reducers/register.reducer';
import * as login from '@a-redux/reducers/login.reducer';
import { AuthState } from '@a-redux/all-states';

export const getAuthStateSelector = createFeatureSelector<AuthState>(
  'auth'
);

export const getRegisterState = createSelector(
  getAuthStateSelector,
  (state: AuthState) => state.register
);
export const getLoginState = createSelector(
  getAuthStateSelector,
  (state: AuthState) => state.login
);

export const getRegisterStatus = createSelector(
  getRegisterState,
  (state: register.RegisterState) => state.status
);
export const getLoginStatus = createSelector(
  getLoginState,
  (state: login.LoginState) => state.status
);
