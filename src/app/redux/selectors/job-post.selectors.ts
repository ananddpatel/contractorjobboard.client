import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as postJob from '@a-redux/reducers/post-job.reducer';
import * as login from '@a-redux/reducers/login.reducer';
import { RecruiterState } from '@a-redux/all-states';

export const getRecruiterStateSelector = createFeatureSelector<RecruiterState>(
  'recruiter'
);

export const getPostJobState = createSelector(
  getRecruiterStateSelector,
  (state: RecruiterState) => state.postJob
);

export const getPostJobStatus = createSelector(
  getPostJobState,
  (state: postJob.PostJobState) => state.status
);
