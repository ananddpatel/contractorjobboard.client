import { MetaReducer } from '@ngrx/store';
import { environment } from '../../environments/environment';
import { storeFreeze } from 'ngrx-store-freeze';

import * as root from '@a-redux/reducers/root.reducer';
import * as register from '@a-redux/reducers/register.reducer';
import * as login from '@a-redux/reducers/login.reducer';
import { PostJobState } from '@a-redux/reducers/post-job.reducer';
import { AlertState } from '@a-redux/reducers/alert.reducer';


export interface IAppState {
  root: RootState;
  auth: AuthState;
  recruiter: RecruiterState;
}
export interface RootState {
  root: root.RootState;
  alert: AlertState;
}
export interface AuthState {
  register: register.RegisterState;
  login: login.LoginState;
}
export interface RecruiterState {
  postJob: PostJobState;
}
