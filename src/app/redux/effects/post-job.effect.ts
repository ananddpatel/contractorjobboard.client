import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Actions, Effect, ofType } from '@ngrx/effects';
import * as postJob from '@a-redux/actions/post-job.actions';
import { catchError, switchMap, map, mergeMap, tap } from 'rxjs/operators';
import { JobService } from '../../common/services/job.service';
import { Action } from '@ngrx/store';
import { AlertSuccess, AlertFail } from '@a-redux/actions/alert.actions';
import { Messages } from '../../common/enums/messages.enum';

@Injectable()
export class PostJobEffects {

  @Effect()
  postJob$: Observable<Action> = this.actions$
  .pipe(ofType(postJob.POST_JOB), mergeMap((action: postJob.PostJob) => {
    return this.jobService.postJob(action.job)
      .pipe(
        mergeMap((res: Object) => {
          // return new postJob.PostJobSuccess();
          return [
            new postJob.PostJobSuccess(),
            new AlertSuccess(Messages.POST_JOB_SUCCESS)
          ];
        }),
        catchError(err => {
          return of(new postJob.PostJobFail(err));
        }));
  }));

  @Effect()
  catchPostJobError$: Observable<Action> = this.actions$
    .pipe(ofType(postJob.POST_JOB_FAIL), map((action: postJob.PostJobFail) => {
      const message = action.error.exists ? Messages.POST_JOB_FAIL_EXISTS : Messages.POST_JOB_FAIL_DEFAULT;
      return new AlertFail(message);
    }));

  constructor(private actions$: Actions, private jobService: JobService) {}
}
