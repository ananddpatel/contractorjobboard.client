import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import * as register from '@a-redux/actions/register.actions';
import * as login from '@a-redux/actions/login.actions';
import { tap, map, mergeMap, catchError, switchMap } from 'rxjs/operators';
import { AuthService } from '../../pages/auth/services/auth.service';
import { UserRoles } from '../../common/enums/user-roles.enum';
import { Router } from '@angular/router';
import { StartupService } from '../../common/services/startup.service';
import { SetUserData } from '@a-redux/actions/root.actions';
import { GetUserProfile } from '@a-redux/actions/user.actions';

@Injectable()
export class AuthEffects {

  @Effect()
  $registerUser: Observable<Action> = this.actions$
  .pipe(ofType(register.REGISTER_USER), switchMap((action: register.RegisterUser) => {
    const ud = action.userData;
    return this.authService.registerUser(ud.firstname, ud.lastname, ud.email, ud.matchingPassword.password, ud.role)
      .pipe(mergeMap(res => {
        // console.log(res);
        return [
          new register.RegisterUserSuccess(),
          ...this.handleLoginSuccessAction(res.token)
        ];
      }), catchError(err => of(new register.RegisterUserFail())));
  }));

  @Effect()
  $loginUser: Observable<Action> = this.actions$
  .pipe(ofType(login.LOGIN_USER), switchMap((action: login.LoginUser) => {
    const ud = action.userData;
    return this.authService.loginUser(ud.email, ud.password)
      .pipe(mergeMap(res => {
        return this.handleLoginSuccessAction(res.token);
      }), catchError(err => of(new login.LoginUserFail())));
  }));

  @Effect({dispatch: false})
  $successfulAuthentication: any = this.actions$
  .pipe(ofType(login.LOGIN_USER_SUCCESS), map(() => {
    this.router.navigate(['/']);
  }));

  @Effect({dispatch: false})
  $logoutUser: any = this.actions$
  .pipe(ofType(login.LOGOUT_USER), map(() => {
    localStorage.removeItem('token');
    this.router.navigate(['/']);
  }));

  constructor(private actions$: Actions, private authService: AuthService, private startUpServ: StartupService, private router: Router) {}

  private handleLoginSuccessAction(token: string): Action[] {
    localStorage.setItem('token', token);
    const decoded = this.startUpServ.decodeJTW(token);
    const expriresAt = decoded.exp * 1000;
    return [new login.LoginUserSuccess(), new SetUserData(decoded.role, expriresAt), new GetUserProfile()];
  }
}
