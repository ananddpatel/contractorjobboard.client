import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Actions, Effect, ofType } from '@ngrx/effects';
import * as user from '@a-redux/actions/user.actions';
import { catchError, switchMap, map } from 'rxjs/operators';
import { ProfileService } from '../../common/services/profile.service';
import { UserProfile } from '../../common/models/user-profile.model';

@Injectable()
export class UserEffects {

  @Effect()
  getUserProfile$: Observable<user.Actions> = this.actions$
  .pipe(ofType(user.GET_USER_PROFILE), switchMap((action: user.GetUserProfile) => {
    return this.profileService.getProfile()
      .pipe(
        map((res: UserProfile) => {
          return new user.GetUserProfileSuccess(res);
        }),
        catchError(err => of(new user.GetUserProfileFail())));
  }));

  @Effect()
  updateUserProfile$: Observable<user.Actions> = this.actions$
  .pipe(ofType(user.UPDATE_USER_PROFILE), switchMap((action: user.UpdateUserProfile) => {
    return this.profileService.updateProfile(action.profile)
      .pipe(
        map((res: UserProfile) => {
          return new user.UpdateUserProfileSuccess(res);
        }),
        catchError(err => of(new user.UpdateUserProfileFail())));
  }));

  constructor(private actions$: Actions, private profileService: ProfileService) {}
}
