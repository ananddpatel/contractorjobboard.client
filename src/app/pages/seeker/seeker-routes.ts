import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SeekerProfilePageComponent } from './seeker-profile-page/seeker-profile-page.component';
import { QuickCvPageComponent } from './quick-cv-page/quick-cv-page.component';
import { SavedJobsPageComponent } from './saved-jobs-page/saved-jobs-page.component';
import { AppliedJobsPageComponent } from './applied-jobs-page/applied-jobs-page.component';

const routes: Routes = [
    { path: 'profile', component: SeekerProfilePageComponent },
    { path: 'profile/quick-cv', component: QuickCvPageComponent },
    { path: 'jobs/saved', component: SavedJobsPageComponent },
    { path: 'jobs/applied', component: AppliedJobsPageComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SeekerRoutingModule { }

// export const routedComponents = [LoginPageComponent, RegisterPageComponent];
