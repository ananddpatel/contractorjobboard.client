import { Component, OnInit } from '@angular/core';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Store, select } from '@ngrx/store';
import { userProfile } from '@a-redux/selectors/root.selectors';
import { take, skipWhile } from 'rxjs/operators';
import { RootState, IAppState } from '../../../redux';
import { SeekerAvailablity } from '../../../common/enums/seeker-availability.enum';
import { Industries } from '../../../common/enums/industries.enum';
import { U } from '../../../common/utils';
import { UpdateUserProfile } from '@a-redux/actions/user.actions';
import { UserProfile } from '../../../common/models/user-profile.model';


@Component({
  selector: 'app-seeker-profile-page',
  templateUrl: './seeker-profile-page.component.html',
  styleUrls: ['./seeker-profile-page.component.css']
})
export class SeekerProfilePageComponent implements OnInit {
  profileForm: FormGroup;
  availability = SeekerAvailablity;
  industries = Industries;
  industriesOpts;
  constructor(private store: Store<any>, private fb: FormBuilder) {

  }

  ngOnInit() {
    this.store.pipe(select(userProfile), skipWhile(profile => !profile), take(1)).subscribe(profile => {
      // console.log(profile);
      // console.log(this.industries);

      this.industriesOpts = Object.keys(this.industries).map((ind, i) => {
        return {
          label: this.industries[ind].display,
          value: ind
        };
      });
      this.profileForm = this.fb.group({
        firstName: [profile.firstName, [Validators.required]],
        lastName: [profile.lastName, [Validators.required]],
        email: [profile.email, [Validators.required, Validators.email]],
        phone: [profile.phone, [Validators.required]],
        dob: [U.isoDateInputFormat(profile.dob)],
        currEmployer: [profile.currEmployer, [Validators.required]],
        currContractStart: [U.isoDateInputFormat(profile.currContractStart), [Validators.required]],
        currContractEnd: [U.isoDateInputFormat(profile.currContractEnd), [Validators.required]],
        availability: [profile.availability, [Validators.required]],
        preferredPayRate: [profile.preferredPayRate ? profile.preferredPayRate : 0, [Validators.required]],
        linkedInProfile: [profile.linkedInProfile],
        location: [profile.location, [Validators.required]],
        preferredDesignation: [profile.preferredDesignation],
        preferredIndustry: [profile.preferredIndustry],
        preferredLocation: [profile.preferredLocation],
      });
    });
  }


  onProfileSave(form: FormGroup) {
    const changedControls = U.objFilter(form.controls, item => !item.pristine);
    const updatedProfile = {} as UserProfile;
    Object.keys(changedControls).forEach(field => {
      return updatedProfile[field] = changedControls[field].value;
    });
    this.store.dispatch(new UpdateUserProfile(updatedProfile));
  }
}
