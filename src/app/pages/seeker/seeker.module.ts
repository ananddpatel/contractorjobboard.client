import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SeekerProfilePageComponent } from './seeker-profile-page/seeker-profile-page.component';
import { QuickCvPageComponent } from './quick-cv-page/quick-cv-page.component';
import { SavedJobsPageComponent } from './saved-jobs-page/saved-jobs-page.component';
import { AppliedJobsPageComponent } from './applied-jobs-page/applied-jobs-page.component';
import { SeekerRoutingModule } from './seeker-routes';
import { EffectsModule } from '@ngrx/effects';
import { UserEffects } from '@a-redux/effects/user.effect';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MultiSelectModule } from 'primeng/multiselect';

@NgModule({
  imports: [
    CommonModule,
    SeekerRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    EffectsModule.forFeature([UserEffects]),
    MultiSelectModule
  ],
  declarations: [SeekerProfilePageComponent, QuickCvPageComponent, SavedJobsPageComponent, AppliedJobsPageComponent]
})
export class SeekerModule { }
