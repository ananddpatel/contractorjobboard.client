import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RecruiterRoutingModule } from './recruiter-routes';
import { CandidateSearchPageComponent } from './candidate-search-page/candidate-search-page.component';
import { RosterPageComponent } from './roster-page/roster-page.component';
import { PostingsPageComponent } from './postings-page/postings-page.component';
import { SubmitJobPageComponent } from './submit-job-page/submit-job-page.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { PostJobEffects } from '@a-redux/effects/post-job.effect';
import { recruiterReducers } from '@a-redux/all-reducers-maps';
import { ElementsModule } from '../../elements/elements.module';

@NgModule({
  imports: [
    CommonModule,
    ElementsModule,
    FormsModule,
    ReactiveFormsModule,
    RecruiterRoutingModule,
    StoreModule.forFeature('recruiter', recruiterReducers),
    EffectsModule.forFeature([PostJobEffects])
  ],
  declarations: [
    CandidateSearchPageComponent,
    SubmitJobPageComponent,
    RosterPageComponent,
    PostingsPageComponent,
    SubmitJobPageComponent
  ]
})
export class RecruiterModule { }
