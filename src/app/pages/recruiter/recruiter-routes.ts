import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SubmitJobPageComponent } from './submit-job-page/submit-job-page.component';
import { CandidateSearchPageComponent } from './candidate-search-page/candidate-search-page.component';
import { RosterPageComponent } from './roster-page/roster-page.component';
import { PostingsPageComponent } from './postings-page/postings-page.component';

const routes: Routes = [
    { path: 'jobs/submit', component: SubmitJobPageComponent },
    { path: 'search/candidate', component: CandidateSearchPageComponent },
    { path: 'roster', component: RosterPageComponent },
    { path: 'jobs/my-postings', component: PostingsPageComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RecruiterRoutingModule { }

// export const routedComponents = [LoginPageComponent, RegisterPageComponent];
