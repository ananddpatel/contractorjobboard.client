import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Skill } from '../../../common/models/skill.model';
import { Store, select } from '@ngrx/store';
import { PostJob } from '@a-redux/actions/post-job.actions';
import { getPostJobStatus } from '@a-redux/selectors/job-post.selectors';
import { Status } from '../../../common/enums/status.enum';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-submit-job-page',
  templateUrl: './submit-job-page.component.html',
  styleUrls: ['./submit-job-page.component.css']
})
export class SubmitJobPageComponent implements OnInit {
  status = Status;
  submitJobForm: FormGroup;
  skills: string[];
  postJobStatus$: Observable<Status>;

  constructor(private fb: FormBuilder, private store: Store<any>) {
    this.skills = [];
    this.postJobStatus$ = this.store.pipe(select(getPostJobStatus));
    this.submitJobForm = this.fb.group({
      jobId: [null, [Validators.required, Validators.minLength(1)]],
      title: [null, [Validators.required, Validators.minLength(1)]],
      description: [null, [Validators.required, Validators.minLength(1)]],
      employer: [{value: null, disabled: false}, Validators.required],
      isConfidential: [false, Validators.required],
      startDate: [],
      endDate: [],
      minRate: [{value: null}, Validators.required],
      maxRate: [{value: null}, Validators.required],
      skills: [],
      applicationCloseDate: [],
    });
  }

  ngOnInit() {
    this.submitJobForm.valueChanges.subscribe(val => {
      this.toggleEmployer(val.isConfidential);
      // console.log(val);

    });
  }

  onJobSubmit(_form) {
    const form = {..._form};
    form.skills = this.skills;
    if (form.isConfidential) {
      form.employer = this.submitJobForm.get('employer').value;
    }
    // console.log(form);
    this.store.dispatch(new PostJob(form));
  }

  addSkill(_skill: string) {
    let skill;
    if (_skill) {
      skill = _skill.trim();
    }
    if (_skill && skill !== '') {
      this.skills.push(skill);
    }
    this.submitJobForm.get('skills').reset();
  }

  toggleEmployer(isConfidential) {
    const employerControl = this.submitJobForm.get('employer');
    if (isConfidential && !employerControl.disabled) {
      employerControl.disable();
    } else if (!isConfidential && employerControl.disabled) {
      employerControl.enable();
    }
  }

  removeSkill(index: number) {
    this.skills.splice(index, 1);
  }

}
