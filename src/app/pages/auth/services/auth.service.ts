import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'environments/environment';
import { UserRoles } from '../../../common/enums/user-roles.enum';

@Injectable()
export class AuthService {

  constructor(private http: HttpClient) {}

  public registerUser(firstName: string, lastName: string, email: string, password: string, role: UserRoles): Observable<any> {
    const url = environment.apiBase + environment.registerResource;
    return this.http.post(url, {
      firstName: firstName,
      lastName: lastName,
      email: email,
      password: password,
      role: role
    });
  }

  public loginUser(email: string, password: string): Observable<any> {
    const url = environment.apiBase + environment.loginResource;
    return this.http.post(url, {
      email: email,
      password: password
    });
  }
}
