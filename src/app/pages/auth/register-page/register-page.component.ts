import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { RegisterUser } from '@a-redux/actions/register.actions';
import { UserRoles } from '../../../common/enums/user-roles.enum';

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.css']
})
export class RegisterPageComponent implements OnInit {
  roles = UserRoles;
  registerForm: FormGroup;
  constructor(private fb: FormBuilder, private route: ActivatedRoute, private store: Store<any>) {
    this.registerForm = this.fb.group({
      firstname: [null, [Validators.required]],
      lastname: [null, [Validators.required]],
      role: [UserRoles.SEEKER, [Validators.required]],
      email: [null, [Validators.required, Validators.email]],
      matchingPassword: this.fb.group({
        password: ['', [Validators.required, Validators.minLength(6)]],
        repeatPassword: ['', Validators.required]
      }, {validator: this.areEqual})
    });
  }

  ngOnInit() {
  }

  areEqual(group: FormGroup) {
    let value;
    let valid = true;
    for (const key in group.controls) {
      if (group.controls.hasOwnProperty(key)) {
        const control = group.controls[key];

        if (value === undefined) {
          value = control.value;
        } else {
          if (value !== control.value) {
            valid = false;
            break;
          }
        }
      }
    }

    if (valid) {
      return null;
    }

    return {
      areEqual: true
    };
  }

  register(form: FormGroup) {
    // console.log(form);
    this.store.dispatch(new RegisterUser(form));
  }

}
