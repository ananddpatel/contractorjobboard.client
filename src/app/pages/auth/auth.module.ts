import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginPageComponent } from './login-page/login-page.component';
import { RegisterPageComponent } from './register-page/register-page.component';
import { StoreModule } from '@ngrx/store';
import { authReducers } from '../../redux';
import { EffectsModule } from '@ngrx/effects';
import { AuthEffects } from '@a-redux/effects/auth.effect';
import { AuthRoutingModule } from './auth-routes';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthService } from './services/auth.service';

@NgModule({
  imports: [
    CommonModule,
    AuthRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    StoreModule.forFeature('auth', authReducers),
    EffectsModule.forFeature([AuthEffects])
  ],
  declarations: [LoginPageComponent, RegisterPageComponent],
  providers: [AuthService]
})
export class AuthModule { }
