import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { tap, take } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { LoginUser } from '@a-redux/actions/login.actions';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {
  loginForm: FormGroup;
  role: string;
  constructor(private fb: FormBuilder, private route: ActivatedRoute, private store: Store<any>) {
    this.loginForm = this.fb.group({
      // tslint:disable-next-line:max-line-length
      email: [null, [Validators.required, Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]],
      password: [null, Validators.required]
    });
  }

  ngOnInit() {
    this.route.paramMap.pipe(take(1)).subscribe(params => {
      this.role = params.get('role');
   });
  }

  login(formValue) {
    this.store.dispatch(new LoginUser(formValue));
  }

}
