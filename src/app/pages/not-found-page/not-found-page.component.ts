import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-not-found-page',
  template: `<h1 class="text-center mt-5 pt-5">404 Not Found</h1>`
})
export class NotFoundPageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
