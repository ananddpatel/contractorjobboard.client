import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { StartupService } from './common/services/startup.service';
import { SetUserData } from '@a-redux/actions/root.actions';
import { isTokenExpired } from '@a-redux/selectors/root.selectors';
import { getRegisterStatus } from '@a-redux/selectors/auth.selectors';
import { GetUserProfile } from '@a-redux/actions/user.actions';

@Component({
  selector: 'app-root',
  template: `<router-outlet></router-outlet>`
})
export class AppComponent implements OnInit {
  constructor(private store: Store<any>, private startUpServ: StartupService) {
    const token = localStorage.getItem('token');
    if (token) {
      const decoded = this.startUpServ.decodeJTW(token);
      const expriresAt = decoded.exp * 1000;
      this.store.dispatch(new SetUserData(decoded.role, expriresAt));
      this.store.dispatch(new GetUserProfile());
    }
  }

  ngOnInit() {
  }
}
